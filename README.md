##Disclaimer:
This has only been tested on macOS. `virtualenv` might work differently on other OS'es, so you might have to consult the documentation.

##Setup
To setup an environment to write WL2 in, you first have to install `virtualenv` and `python2.7`. Once you have that installed, run these in the repository:

```
virtualenv --python={path/to/python2.7} --prompt="(workflow-lang-v2)" env
source env/bin/activate
pip install -r requirements.txt
```

##Write WL2
To write WL2, you can either:

 - open a Python console while in your `virtualenv`
 - open a Python file in an editor/IDE that uses `$REPO/env/bin/python2.7` as its Python interpreter

The normal writing functionality can be imported from `workflow_lang.frontend`.
The translation functionality (given a workflow written in WL1) can be imported from `translator.translator`.
The few parts of the runtime (particularly the functionality used to run serialized Python expressions) can be imported through `from workflow_runtime.code import run_code`.

##Sample Bundles
3 sample bundles can be found in the `sample_bundles` directory. Together they form an simplified and stubbed implementation of an incremental deployment Workflow.
Running `PYTHONPATH=. python sample_bundles/incremental_deployment.py` will show that `incremental_deployment` does compile to a Concrete Syntax Tree (the rest of the compilation pipeline is mostly untouched and not available in this repository).

##Serialized Expressions
3 sample serialized expressions can be found in `serialized_expressions_demo/serialized_expressions.py` and run using `PYTHONPATH=. python serialized_expressions_demo/serialized_expressions.py`.