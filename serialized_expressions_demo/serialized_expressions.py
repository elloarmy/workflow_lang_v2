from workflow_lang.expressions import Expr
from workflow_lang.utils import serialize_expression
from workflow_runtime.code import run_code


def execute_expression(expr, scope):
    print 'Executing {expr} with scope {scope} results in {res}'.format(
        expr=serialize_expression(expr, pretty=True), scope=scope,
        res=run_code(serialize_expression(expr), scope))

if __name__ == '__main__':
    lst = Expr('lst')
    list_lookup_expression = lst[5:]
    scope = {'lst': range(10)}
    execute_expression(list_lookup_expression, scope)

    dct = Expr('dct')
    dct_lookup_expression = dct['value']
    scope = {'dct': {'value': 10}}
    execute_expression(dct_lookup_expression, scope)

    func = Expr('func')
    dct_lookup_expression = func(2)
    scope = {'func': lambda x: x + 5}
    execute_expression(dct_lookup_expression, scope)
