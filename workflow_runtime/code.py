import re
import urllib
import uuid
from datetime import (
    datetime,
    timedelta
)

from workflow_lang.expressions import is_serialized_expression
from workflow_runtime.expr_deserializer import (
    parse_expr,
    stringify_expr
)


_IDENTIFIER_REGEX = re.compile(r'^[a-zA-Z_][a-zA-Z0-9_]*$')


def run_code(expr, scope):
    global_scope = utils()
    global_scope.update(scope)
    local_scope = {}
    try:
        if is_serialized_expression(expr):
            return parse_expr(expr, global_scope)
        assign = 'readout = %s' % expr
        exec assign in global_scope, local_scope
        return local_scope['readout']
    except (UnicodeEncodeError,
            SyntaxError,
            IndexError,
            NameError,
            KeyError,
            AttributeError,
            TypeError,
            StopIteration) as e:
        msg_template = 'Unable to execute expression: {exp} due to {err} in scope {s}'
        if is_serialized_expression(expr):
            expr = stringify_expr(expr)
        msg = msg_template.format(exp=expr, err=e, s=scope)
        raise Exception(msg)


def utils():
    return {'uuid4': uuid.uuid4,
            'urlencode': urllib.quote,
            'utcnow': datetime.utcnow,
            'timedelta': timedelta,
            'utcfromtimestamp': _utcfromtimestamp,
            }


def _utcfromtimestamp(value):
    if isinstance(value, datetime):
        return value
    else:
        return datetime.utcfromtimestamp(value)