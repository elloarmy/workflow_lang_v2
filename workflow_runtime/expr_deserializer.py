import __builtin__
import operator
from types import NoneType

from workflow_lang.constants import (
    ACTION_EXPR,
    ATTRIBUTE_EXPR,
    BOOLEAN_EXPR,
    CONTAINS,
    EXPR,
    EXPR_TYPE,
    GLOBALS_EXPR,
    LIST_EXPR,
    LOOKUP_EXPR,
    NOT_EXPR,
    OPERATOR_EXPR,
    SIMPLEDICT_EXPR,
    SIMPLESET_EXPR,
    STRING_EXPR
)

translate_method = {
    '__lt__': '<',
    '__le__': '<=',
    '__eq__': '==',
    '__ne__': '!=',
    '__ge__': '>=',
    '__gt__': '>',
    '__add__': '+',
    '__and__': '&',
    '__div__': '/',
    '__floordiv__': '//',
    '__mod__': '%',
    '__mul__': '*',
    '__or__': '|',
    '__pow__': '**',
    '__sub__': '-',
    '__xor__': '^',
    '__lshift__': '<<',
    '__rshift__': '>>',
    '__iadd__': '+=',
    '__iand__': '&=',
    '__idiv__': '/=',
    '__ifloordiv__': '//=',
    '__ilshift__': '<<=',
    '__imod__': '%=',
    '__imul__': '*=',
    '__ior__': '|=',
    '__ipow__': '**=',
    '__irshift__': '>>=',
    '__isub__': '-=',
    '__ixor__': '^=',
    '__radd__': '+',
    '__rand__': '&',
    '__rdiv__': '/',
    '__rfloordiv__': '//',
    '__rlshift__': '<<',
    '__rmod__': '%',
    '__rmul__': '*',
    '__ror__': '|',
    '__rpow__': '**',
    '__rrshift__': '>>',
    '__rsub__': '-',
    '__rxor__': '^'
}


def update_scope(elem, vars, dict2):
    if isinstance(vars, (list, tuple, set)) and isinstance(elem, (list, tuple, set)) and len(
            vars) == len(elem):
        dict1 = {v: i for v, i in zip(vars, elem)}
    elif isinstance(vars, (list, tuple, set)) and len(vars) == 1:
        dict1 = {vars[0]: elem}
    elif type(vars) is not list:
        dict1 = {vars: elem}
    else:
        raise Exception('Amount of variables in iter doesnt correspond')
    dict1.update(dict2)
    return dict1


def handle_boolexpr(expr, scope):
    iff = _parse_expr(expr['if'], scope)
    if iff:
        return _parse_expr(expr['then'], scope)
    else:
        return _parse_expr(expr['else'], scope)


def handle_listexpr(expr, scope):
    res = []

    elem_expr = expr['expr']
    lists = expr['lists']
    vars = expr['vars']
    cond = expr['cond']
    temp_scope = {}
    temp_scope.update(scope)

    def iterate(lists, vars, temp_scope):
        lst = lists[0]
        var = vars[0]
        lst = _parse_expr(lst, temp_scope) if type(lst) is not list else lst
        for elem in lst:
            if len(lists) != 1:
                iterate(lists[1:], vars[1:], update_scope(elem, var, temp_scope))
            elif _parse_expr(cond, update_scope(elem, var, temp_scope)):
                res.append(_parse_expr(elem_expr, update_scope(elem, var, temp_scope)))

    iterate(lists, vars, temp_scope)
    return res


def handle_simplesetexpr(expr, scope):
    elem_expr = expr['expr']
    st = _parse_expr(expr['set'], scope)
    vars = expr['vars']
    cond = expr['cond']
    return {_parse_expr(elem_expr, update_scope(elem, vars, scope)) for elem in st if
            _parse_expr(cond, update_scope(elem, vars, scope))}


def handle_simpledictexpr(expr, scope):
    keyexpr = expr['keyexpr']
    valexpr = expr['valexpr']
    dct = _parse_expr(expr['dict'], scope)
    vars = expr['vars']
    cond = expr['cond']
    return {_parse_expr(keyexpr, update_scope(elem,
                                              vars,
                                              scope)): _parse_expr(valexpr, update_scope(elem,
                                                                                         vars,
                                                                                         scope))
            for elem in dct if
            _parse_expr(cond, update_scope(elem, vars, scope))}


def handle_simple_expr(expr, scope):
    return scope[expr['name']]


def handle_global_expr(expr, _):
    return __builtin__.__getattribute__(expr['name'])


def handle_string_expr(expr, _):
    return expr['string']


def handle_action_expr(expr, scope):
    caller = _parse_expr(expr['caller'], scope)
    args = [_parse_expr(arg, scope) for arg in expr['args']]
    kwargs = expr['kwargs']

    actual_args = _parse_expr(kwargs.pop('unpack_list'),
                              scope) if 'unpack_list' in kwargs else []
    combined_kwargs = _parse_expr(kwargs.pop('unpack_dict'),
                                  scope) if 'unpack_dict' in kwargs else {}
    combined_kwargs.update({k: _parse_expr(v, scope) for k, v in kwargs.iteritems()})
    return caller.__call__(*args + actual_args, **combined_kwargs)


def handle_attribute_expr(expr, scope):
    caller = _parse_expr(expr['caller'], scope)
    item = _parse_expr(expr['item'], scope)
    return caller.__getattribute__(item)


def handle_lookup_expr(expr, scope):
    caller = _parse_expr(expr['caller'], scope)
    key = _parse_expr(expr['key'], scope)
    return caller.__getitem__(key)


def handle_operator_expr(expr, scope):
    caller = _parse_expr(expr['caller'], scope)
    other = _parse_expr(expr['other'], scope)
    method = expr['method']
    if method == 'and':
        return caller and other
    elif method == 'or':
        return caller or other
    elif method == 'in':
        return caller in other
    elif method == 'is':
        return caller is other
    elif method == 'not in':
        return caller not in other
    elif method == 'is not':
        return caller is not other

    if hasattr(caller, method):
        return caller.__getattribute__(method)(other)
    else:
        return operator.__getattribute__(method)(caller, other)


def handle_not_expr(expr, scope):
    return not _parse_expr(expr['var'], scope)


visit_parser = {EXPR: handle_simple_expr,
                GLOBALS_EXPR: handle_global_expr,
                STRING_EXPR: handle_string_expr,
                BOOLEAN_EXPR: handle_boolexpr,
                LIST_EXPR: handle_listexpr,
                SIMPLESET_EXPR: handle_simplesetexpr,
                SIMPLEDICT_EXPR: handle_simpledictexpr,
                NOT_EXPR: handle_not_expr,
                ACTION_EXPR: handle_action_expr,
                ATTRIBUTE_EXPR: handle_attribute_expr,
                LOOKUP_EXPR: handle_lookup_expr,
                OPERATOR_EXPR: handle_operator_expr}


def handle_simple_type(expr, _):
    return expr


def handle_list_type(expr, scope):
    return [_parse_expr(v, scope) for v in expr]


def handle_set_type(expr, scope):
    return {_parse_expr(v, scope) for v in expr}


def handle_tuple_type(expr, scope):
    return tuple([_parse_expr(v, scope) for v in expr])


type_visitor = {NoneType: handle_simple_type,
                int: handle_simple_type,
                str: handle_simple_type,
                unicode: handle_simple_type,
                bool: handle_simple_type,
                list: handle_list_type,
                set: handle_set_type,
                tuple: handle_tuple_type}


def _parse_expr(expr, scope):
    v_type = type(expr)
    if v_type in type_visitor:
        return type_visitor[v_type](expr, scope)

    if EXPR_TYPE not in expr:  # Still dict, but not serialized expression
        return {_parse_expr(d['key'], scope): _parse_expr(d['value'], scope) for d in expr['items']}

    expr_type = expr[EXPR_TYPE]
    if expr_type in visit_parser:
        return visit_parser[expr_type](expr, scope)

    raise Exception('Expression type %s not found' % str(expr_type))


def parse_expr(expr, scope):
    """
    Unpacks container and executes the serialized expression
    :param dict expr: dict with single key where corresponding value is serialized expression
    :param dict scope: current scope with mappings between variables and values
    :return: return value from executed expression
    """
    return _parse_expr(expr[CONTAINS], scope)


def stringify_listexpr(expr):
    vars = expr['vars']
    lists = expr['lists']
    cond = expr['cond']
    cond = ' if ' + _stringify_expr(cond) if cond is not True else ''

    return '[' + _stringify_expr(expr['expr']) + ''.join(
        [' for ' + ', '.join(v) + ' in ' + _stringify_expr(lst) for v, lst in
         zip(vars, lists)]) + cond + ']'


def stringify_simplesetexpr(expr):
    cond = expr['cond']
    cond = ' if ' + _stringify_expr(cond) if cond is not True else ''
    return '{' + _stringify_expr(expr['expr']) + ' for ' + ', '.join(
        expr['vars']) + ' in ' + _stringify_expr(expr['set']) + cond + '}'


def stringify_simpledictexpr(expr):
    cond = expr['cond']
    cond = ' if ' + _stringify_expr(cond) if cond is not True else ''
    return '{' + _stringify_expr(expr['keyexpr']) + ': ' + _stringify_expr(
        expr['valexpr']) + ' for ' + ', '.join(expr['vars']) + ' in ' + _stringify_expr(
        expr['dict']) + cond + '}'


def stringify_actionexpr(expr):
    caller = _stringify_expr(expr['caller'])
    args = [_stringify_expr(arg) for arg in expr['args']]
    kwargs = {k: _stringify_expr(v) for k, v in expr['kwargs'].iteritems()}
    potential_comma = ', ' if len(args) > 0 and len(kwargs) > 0 else ''
    return caller + '(' + ', '.join(args) + potential_comma + ', '.join(
        [k + '=' + v for k, v in kwargs.iteritems()]) + ')'


def stringify_attributeexpr(expr):
    return _stringify_expr(expr['caller']) + '.' + expr['item']


def stringify_lookupexpr(expr):
    return _stringify_expr(expr['caller']) + '[' + _stringify_expr(expr['key']) + ']'


def stringify_operatorexpr(expr):
    caller = _stringify_expr(expr['caller'])
    other = _stringify_expr(expr['other'])
    method = expr['method']
    left = caller
    right = other
    if method in translate_method:
        # make sure that operands are placed on the right side of the operator
        if method.startswith('__r') and not method == '__rshift__':
            left = other
            right = caller
        method = translate_method[method]
    return left + ' ' + method + ' ' + right


def handle_simple_type_stringify(expr):
    return str(expr)


def handle_list_type_stringify(expr):
    return '[' + ', '.join([_stringify_expr(v) for v in expr]) + ']'


def handle_tuple_type_stringify(expr):
    return '(' + ', '.join([_stringify_expr(v) for v in expr]) + ')'


string_type_visitor = {NoneType: handle_simple_type_stringify,
                       int: handle_simple_type_stringify,
                       str: lambda expr: '"' + expr.replace('"', '\\"').replace("'", "\\'") + '"',
                       unicode: lambda expr: 'u"' + expr + '"',
                       bool: handle_simple_type_stringify,
                       list: handle_list_type_stringify,
                       tuple: handle_tuple_type_stringify}


def stringify_stringexpr(expr):
    string = expr['string'].replace('"', '\\"').replace("'", "\\'")
    if type(string) is unicode:
        return 'u"' + string + '"'
    else:
        return '"' + string + '"'


string_expr_type_visitor = {EXPR: lambda expr: expr['name'],
                            GLOBALS_EXPR: lambda expr: expr['name'],
                            STRING_EXPR: stringify_stringexpr,
                            BOOLEAN_EXPR: lambda expr: _stringify_expr(
                                expr['then']) + ' if ' + _stringify_expr(
                                expr['if']) + ' else ' + _stringify_expr(
                                expr['else']),
                            LIST_EXPR: stringify_listexpr,
                            SIMPLESET_EXPR: stringify_simplesetexpr,
                            SIMPLEDICT_EXPR: stringify_simpledictexpr,
                            NOT_EXPR: lambda expr: 'not ' + _stringify_expr(expr['var']),
                            ACTION_EXPR: stringify_actionexpr,
                            ATTRIBUTE_EXPR: stringify_attributeexpr,
                            LOOKUP_EXPR: stringify_lookupexpr,
                            OPERATOR_EXPR: stringify_operatorexpr}


def _stringify_expr(expr):
    v_type = type(expr)

    if v_type in string_type_visitor:
        return string_type_visitor[v_type](expr)

    if EXPR_TYPE not in expr:  # Still dict, but not serialized expression
        return '{' + ', '.join(
            [_stringify_expr(d['key']) + ': ' + _stringify_expr(d['value']) for d in
             expr['items']]) + '}'

    expr_type = expr[EXPR_TYPE]

    if expr_type in string_expr_type_visitor:
        return string_expr_type_visitor[expr_type](expr)

    raise Exception('Expression type %s not found' % str(expr_type))


def stringify_expr(expr):
    """
    Unpacks container and constructs the string equivalent to the serialized expression
    :param dict expr: dict with single key where corresponding value is serialized expression
    :return: string representation of Python expression
    """
    return _stringify_expr(expr[CONTAINS])
