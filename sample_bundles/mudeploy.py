"""
name: mudeploy
version: 1
maintainers: [eplatz@uber.com]
imports:
  - bundle: chat
"""
from workflow_lang.frontend import (
    LegacyCall,
    Format,
    workflow
)
from workflow_lang.globals import (
    utcnow,
    workflow_user
)


@workflow()
def upgrade(service, deployment, cluster, build_ref):
    return {'success': True}


@workflow()
def transfer(service, deployment, cluster, build_ref):
    return {'success': True}


@workflow()
def monitor(service, deployment, cluster):
    LegacyCall('chat.message', room='workflow-language-v2-workshop',
               message=Format('{wfuser} tried to monitor {svc} at {cluster}:{depl} at time {utc}',
                              depl=deployment, svc=service, wfuser=workflow_user, cluster=cluster,
                              utc=utcnow()),
               color='green')
