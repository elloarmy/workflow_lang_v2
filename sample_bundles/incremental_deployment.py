"""
name: incremental_deployment
version: 1
maintainers: [eplatz@uber.com]
"""
from workflow_lang.frontend import (
    If,
    Loop,
    LoopAsync,
    workflow
)

import sample_bundles.mubuild as mubuild
import sample_bundles.mudeploy as mudeploy


@workflow()
def upgrade_deployment(service, deployment, build_ref, cluster):
    transfer_res = mudeploy.transfer(service, deployment, cluster, build_ref)
    with If(transfer_res['success']):
        upgrade_res = mudeploy.upgrade(service, deployment, cluster, build_ref)
        with If(upgrade_res['success']):
            mudeploy.monitor(service, deployment, cluster)


@workflow()
def rollout_cluster(service, cluster, build_ref):
    with Loop(['prod1', 'prod2', 'prod3', 'prod4']) as deployment:
        upgrade_deployment(service, deployment, build_ref, cluster)


@workflow()
def rollout(service, build_ref):
    with Loop([['DC1', 'DC2'], ['DC3', 'DC4']]) as parallel_clusters:
        with LoopAsync(parallel_clusters) as cluster:
            rollout_cluster(service, cluster, build_ref)


@workflow(main=True)
def incremental_deployment(service, gitref):
    build_res = mubuild.build(service, gitref)
    rollout(service, build_res['build-ref'])


if __name__ == '__main__':
    print incremental_deployment.to_primitive(context='pretty')