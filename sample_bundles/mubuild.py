"""
name: mubuild
version: 1
maintainers: [eplatz@uber.com]
imports:
  - bundle: chat
"""
from workflow_lang.frontend import (
    LegacyCall,
    Format,
    workflow
)
from workflow_lang.globals import (
    utcnow,
    uuid4,
    workflow_user
)


@workflow()
def build(service, gitref):
    LegacyCall('chat.message', room='workflow-language-v2-workshop',
               message=Format('{wfuser} tried to build {svc} from {gitref} at time {utc}',
                              gitref=gitref, svc=service, wfuser=workflow_user, utc=utcnow()),
               color='green')
    return {'build-ref': str(uuid4())}
