import uuid

from workflow_lang.expressions import (
    InsertableExpr,
    Not,
    Expr
)
from workflow_lang.models import (
    Block,
    Conditional,
    Iteration,
    grab,
    release,
    stack
)


class ContextManager(object):
    def __init__(self, obj):
        self._obj = obj

    def __enter__(self):
        self._offset = len(stack)
        grab()
        return self._obj

    def __exit__(self, exc_type, exc_val, exc_tb):
        release()
        self._seq = stack[self._offset:]
        del stack[self._offset:]


class If(ContextManager):
    def __init__(self, condition):
        self._conditional = Conditional()
        self._conditional.condition = condition
        super(If, self).__init__(self._conditional)

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(If, self).__exit__(exc_type, exc_val, exc_tb)
        self._conditional.sequential = self._seq
        self._conditional.validate()


class Else(ContextManager):
    def __init__(self):
        cond = stack[-1]
        if not isinstance(cond, Conditional):
            raise Exception('Else can only be after Conditional')
        self._conditional = Conditional()
        self._conditional.condition = Not(InsertableExpr(cond.condition))
        super(Else, self).__init__(self._conditional)

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(Else, self).__exit__(exc_type, exc_val, exc_tb)
        self._conditional.sequential = self._seq
        self._conditional.validate()


class GenericBlock(ContextManager):
    def __init__(self, parallel):
        self._block = Block(parallel=parallel)
        super(GenericBlock, self).__init__(self._block)

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(GenericBlock, self).__exit__(exc_type, exc_val, exc_tb)
        self._block.statements = self._seq
        self._block.validate()


class Parallel(GenericBlock):
    def __init__(self):
        super(Parallel, self).__init__(True)


class Sequential(GenericBlock):
    def __init__(self):
        super(Sequential, self).__init__(False)


class Iterate(ContextManager):
    def __init__(self, list, parallel):
        self._iteration = Iteration(parallel=parallel)
        self._iteration.list = list
        super(Iterate, self).__init__(self._iteration)

    def __enter__(self):
        super(Iterate, self).__enter__()
        var = 's' + uuid.uuid4().hex
        self._iteration.var = var
        return Expr(var)

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(Iterate, self).__exit__(exc_type, exc_val, exc_tb)
        self._iteration.sequential = self._seq
        self._iteration.validate()


class LoopAsync(Iterate):
    output = []

    @classmethod
    def outputs(cls, expr):
        var = 's' + uuid.uuid4().hex
        cls.output.append((var, expr))
        return Expr(var)

    def __enter__(self):
        self._loop_offset = len(self.output)
        return super(LoopAsync, self).__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        output = self.output[self._loop_offset:]
        self._iteration.output = dict(output)
        del self.output[self._loop_offset:]
        super(LoopAsync, self).__exit__(exc_type, exc_val, exc_tb)

    def __init__(self, list):
        super(LoopAsync, self).__init__(list, True)


class Loop(Iterate):
    def __init__(self, list):
        super(Loop, self).__init__(list, False)
