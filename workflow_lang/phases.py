from workflow_lang.models import (
    Call,
    Phase
)
from translator.reserved import (
    PHASE_CANCEL,
    PHASE_COMPLETE,
    PHASE_FAIL,
    PHASE_PAUSE,
    PHASE_ROLLBACK,
    PHASE_START,
    PHASE_UPDATE
)


def create_phase(phase_type, key, context, notification, display_data):
    phase = Phase(stmt=phase_type, raw_data={'key': key,
                                             'ctx': context,
                                             'display_data': display_data,
                                             'notification': notification})
    Call(raw_data={'call': 'stdlib.noop', 'phase': phase})
    return phase


def phase_start(key, context, display_data=None, notification=None):
    return create_phase(PHASE_START, key, context, notification, display_data)


def phase_complete(key, context, display_data=None, notification=None):
    return create_phase(PHASE_COMPLETE, key, context, notification, display_data)


def phase_fail(key, context, display_data=None, notification=None):
    return create_phase(PHASE_FAIL, key, context, notification, display_data)


def phase_cancel(key, context, display_data=None, notification=None):
    return create_phase(PHASE_CANCEL, key, context, notification, display_data)


def phase_rollback(key, context, display_data=None, notification=None):
    return create_phase(PHASE_ROLLBACK, key, context, notification, display_data)


def phase_pause(key, context, display_data=None, notification=None):
    return create_phase(PHASE_PAUSE, key, context, notification, display_data)


def phase_update(key, context, display_data=None, notification=None):
    return create_phase(PHASE_UPDATE, key, context, notification, display_data)
