from workflow_lang.constants import CONTAINS
from workflow_lang.expressions import (
    is_serialized_expression,
    try_serialize
)
from workflow_runtime.expr_deserializer import stringify_expr


def serialize_expression(expr, pretty=False):
    if is_serialized_expression(expr):
        return stringify_expr(expr) if pretty else expr

    return stringify_expr(serialize_expression(expr)) if pretty else {CONTAINS: try_serialize(expr)}
