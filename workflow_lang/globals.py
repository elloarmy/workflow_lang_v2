# Used in 'from udeploy_orchestrator.lib.workflow_lang.builder.frontend.globals import *'
from datetime import (  # noqa
    datetime,
    timedelta
)
from urllib import quote as urlencode  # noqa
from uuid import uuid4  # noqa

from workflow_runtime.code import _utcfromtimestamp as utcfromtimestamp  # noqa

response_code = 200
execution_id = 'dfa9f323-fda0-4135-a4c5-dc9c32566d96'
response = {}
body = {}
workflow_user = 'robot@fakedomain.com'
signaling_msg = 'not sure what this should be'
utcnow = datetime.utcnow
