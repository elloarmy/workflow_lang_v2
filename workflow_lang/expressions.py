from workflow_lang.constants import (
    ACTION_EXPR,
    ATTRIBUTE_EXPR,
    CONTAINS,
    BOOLEAN_EXPR,
    DICT_EXPR,
    EXPR,
    EXPR_TYPE,
    GLOBALS_EXPR,
    LIST_EXPR,
    LOOKUP_EXPR,
    NOT_EXPR,
    OPERATOR_EXPR,
    SIMPLEDICT_EXPR,
    SIMPLESET_EXPR,
    STRING_EXPR
)

type_dict = {
    list: lambda v: [try_serialize(var) for var in v],
    tuple: lambda v: tuple([try_serialize(var) for var in v]),
    dict: lambda v: {'items': [{'key': try_serialize(k), 'value': try_serialize(var)} for k, var in
                               v.iteritems()]},
    set: lambda v: GlobalsExpr('set')([try_serialize(i) for i in v]).serialized(),
}


def try_serialize(expr):
    """
    Serialize expression in regards to its type if possible
    """
    return type_dict.get(type(expr), lambda v: v.serialized() if isinstance(v, Expr) else v)(expr)


def is_serialized_expression(expr):
    return isinstance(expr, dict) and CONTAINS in expr


class Expr(object):
    def __init__(self, name):
        self._name = name

    def __call__(self, *args, **kwargs):
        return ActionExpr(self, *args, **kwargs)

    def __getattr__(self, item):
        return AttributeExpr(self, item)

    def __getitem__(self, key):
        return LookupExpr(self, key)

    def __lt__(self, other):
        return OperatorExpr(self, other, '__lt__')

    def __le__(self, other):
        return OperatorExpr(self, other, '__le__')

    def __eq__(self, other):
        return OperatorExpr(self, other, '__eq__')

    def __ne__(self, other):
        return OperatorExpr(self, other, '__ne__')

    def __gt__(self, other):
        return OperatorExpr(self, other, '__gt__')

    def __ge__(self, other):
        return OperatorExpr(self, other, '__ge__')

    def __add__(self, other):
        return OperatorExpr(self, other, '__add__')

    def __sub__(self, other):
        return OperatorExpr(self, other, '__sub__')

    def __mul__(self, other):
        return OperatorExpr(self, other, '__mul__')

    def __div__(self, other):
        return OperatorExpr(self, other, '__div__')

    def __floordiv__(self, other):
        return OperatorExpr(self, other, '__floordiv__')

    def __mod__(self, other):
        return OperatorExpr(self, other, '__mod__')

    def __pow__(self, other):
        return OperatorExpr(self, other, '__pow__')

    def __lshift__(self, other):
        return OperatorExpr(self, other, '__lshift__')

    def __rshift__(self, other):
        return OperatorExpr(self, other, '__rshift__')

    def __and__(self, other):
        return OperatorExpr(self, other, '__and__')

    def __xor__(self, other):
        return OperatorExpr(self, other, '__xor__')

    def __or__(self, other):
        return OperatorExpr(self, other, '__or__')

    def __radd__(self, other):
        return OperatorExpr(self, other, '__radd__')

    def __rsub__(self, other):
        return OperatorExpr(self, other, '__rsub__')

    def __rmul__(self, other):
        return OperatorExpr(self, other, '__rmul__')

    def __rdiv__(self, other):
        return OperatorExpr(self, other, '__rdiv__')

    def __rfloordiv__(self, other):
        return OperatorExpr(self, other, '__rfloordiv__')

    def __rmod__(self, other):
        return OperatorExpr(self, other, '__rmod__')

    def __rpow__(self, other):
        return OperatorExpr(self, other, '__rpow__')

    def __rlshift__(self, other):
        return OperatorExpr(self, other, '__rlshift__')

    def __rrshift__(self, other):
        return OperatorExpr(self, other, '__rrshift__')

    def __rand__(self, other):
        return OperatorExpr(self, other, '__rand__')

    def __rxor__(self, other):
        return OperatorExpr(self, other, '__rxor__')

    def __ror__(self, other):
        return OperatorExpr(self, other, '__ror__')

    def __ilshift__(self, other):
        other.__dict__[other.varname()].name = self._name
        return self

    def __iter__(self):  # So no infinite loop when unpacking (our modified __getitem__ was culprit)
        return (a for a in [self])

    def __hash__(self):
        return hash(self._name)

    def varname(self):
        return self._name

    def serialized(self):
        return {EXPR_TYPE: EXPR,
                'name': self._name}


class ActionExpr(Expr):
    def __init__(self, caller, *args, **kwargs):
        super(ActionExpr, self).__init__(None)
        self._caller = caller
        self._args = args
        self._kwargs = kwargs

    def serialized(self):
        return {
            EXPR_TYPE: ACTION_EXPR,
            'caller': self._caller.serialized(),
            'args': map(try_serialize, self._args),
            'kwargs': {k: try_serialize(v) for k, v in self._kwargs.iteritems()}
        }


class AttributeExpr(Expr):
    def __init__(self, caller, method):
        super(AttributeExpr, self).__init__(None)
        self._caller = caller
        self._method = method

    def serialized(self):
        return {
            EXPR_TYPE: ATTRIBUTE_EXPR,
            'caller': self._caller.serialized(),
            'item': self._method
        }


class LookupExpr(Expr):
    def __init__(self, caller, key):
        super(LookupExpr, self).__init__(None)
        self._caller = caller
        self._key = GlobalsExpr('slice')(key.start,
                                         key.stop,
                                         key.step) if isinstance(key, slice) else key

    def serialized(self):
        return {
            EXPR_TYPE: LOOKUP_EXPR,
            'caller': self._caller.serialized(),
            'key': try_serialize(self._key)
        }


class OperatorExpr(Expr):
    def __init__(self, caller, other, method):
        super(OperatorExpr, self).__init__(None)
        self._caller = caller
        self._other = other
        self._method = method

    def serialized(self):
        return {
            EXPR_TYPE: OPERATOR_EXPR,
            'caller': try_serialize(self._caller),
            'other': try_serialize(self._other),
            'method': self._method
        }


class BooleanExpr(Expr):
    def __init__(self, ifvar, thenvar, elsevar):
        super(BooleanExpr, self).__init__(None)
        self._ifvar = ifvar
        self._thenvar = thenvar
        self._elsevar = elsevar

    def serialized(self):
        return {
            EXPR_TYPE: BOOLEAN_EXPR,
            'if': try_serialize(self._ifvar),
            'then': try_serialize(self._thenvar),
            'else': try_serialize(self._elsevar)
        }


class StringExpr(Expr):
    def __init__(self, string):
        super(StringExpr, self).__init__(None)
        self._string = string

    def serialized(self):
        return {
            EXPR_TYPE: STRING_EXPR,
            'string': self._string
        }


class ListExpr(Expr):
    def __init__(self, expr, *lists, **kwargs):
        super(ListExpr, self).__init__(None)
        vars, lists = zip(*lists)

        def make_vars(l):
            return [Expr(var) for sublist in l for var in sublist]

        args = make_vars(vars)
        self._vars = list(vars)
        self._lists = [lst(*make_vars(vars[:idx])) for idx, lst in enumerate(lists)]
        self._expr = expr(*args)
        self._cond = kwargs.get('cond', lambda *_: True)(*args)

    def serialized(self):
        return {
            EXPR_TYPE: LIST_EXPR,
            'expr': try_serialize(self._expr),
            'vars': self._vars,
            'lists': try_serialize(self._lists),
            'cond': try_serialize(self._cond)
        }


class DictExpr(Expr):
    def __init__(self, keyexpr, valexpr, *lists, **kwargs):
        super(DictExpr, self).__init__(None)
        vars, lists = zip(*lists)

        def make_vars(l):
            if len(l) == 0:
                return []
            return [Expr(var) for sublist in l for var in sublist]

        args = make_vars(vars)
        self._vars = list(vars)
        self._lists = [lst(*make_vars(vars[:idx])) for idx, lst in enumerate(lists)]
        self._keyexpr = keyexpr(*args)
        self._valexpr = valexpr(*args)
        self._cond = kwargs.get('cond', lambda *_: True)(*args)

    def serialized(self):
        return {
            EXPR_TYPE: DICT_EXPR,
            'keyexpr': try_serialize(self._keyexpr),
            'valexpr': try_serialize(self._valexpr),
            'vars': self._vars,
            'lists': try_serialize(self._lists),
            'cond': try_serialize(self._cond)
        }


class SimpleDictExpr(Expr):
    def __init__(self, keyexpr, valexpr, vars, dct, cond=None):
        super(SimpleDictExpr, self).__init__(None)
        args = [Expr(var) for var in vars]
        self._vars = vars
        self._dict = dct
        self._keyexpr = keyexpr(*args)
        self._valexpr = valexpr(*args)
        self._cond = cond(*args) if cond else True

    def serialized(self):
        return {
            EXPR_TYPE: SIMPLEDICT_EXPR,
            'keyexpr': try_serialize(self._keyexpr),
            'valexpr': try_serialize(self._valexpr),
            'vars': self._vars,
            'dict': try_serialize(self._dict),
            'cond': try_serialize(self._cond)
        }


class SimpleSetExpr(Expr):
    def __init__(self, expr, vars, lst, cond=None):
        super(SimpleSetExpr, self).__init__(None)
        args = [Expr(var) for var in vars]
        self._vars = vars
        self._list = lst
        self._expr = expr(*args)
        self._cond = cond(*args) if cond else True

    def serialized(self):
        return {
            EXPR_TYPE: SIMPLESET_EXPR,
            'expr': try_serialize(self._expr),
            'vars': self._vars,
            'set': try_serialize(self._list),
            'cond': try_serialize(self._cond)
        }


class Not(Expr):
    def __init__(self, var):
        super(Not, self).__init__(None)
        self._var = var

    def serialized(self):
        return {
            EXPR_TYPE: NOT_EXPR,
            'var': try_serialize(self._var)
        }


class GlobalsExpr(Expr):
    def __init__(self, name):
        super(GlobalsExpr, self).__init__(name)

    def serialized(self):
        return {EXPR_TYPE: GLOBALS_EXPR,
                'name': self._name}


class InsertableExpr(Expr):
    def __init__(self, containerized_var):
        super(InsertableExpr, self).__init__(None)
        self._containerized_var = containerized_var

    def serialized(self):
        return self._containerized_var[CONTAINS]
