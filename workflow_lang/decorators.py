import re

import yaml

from workflow_lang.expressions import Expr
from workflow_lang.method_vars import builtin_vars
from workflow_lang.models import (  # noqa
    ActionDefinition,
    Condition,
    Input,
    Request,
    ThriftRequest,
    WorkflowDefinition,
    grab,
    release,
    scope,
    stack
)
from workflow_lang.utils import serialize_expression
from workflow_runtime.code import run_code

type_finder = re.compile(':type (?P<name>\w+): (?P<type>\w+)')
param_finder = re.compile(':param (?:(?P<type>\w+) )?(?P<name>\w+):(?: (?P<desc>.*))?(?:\n|$)')


def extract_ns(docstring):
    try:
        y = yaml.load(docstring)
    except yaml.YAMLError as e:
        raise ValueError(e)
    return y['name'] if 'name' in y else None


def augment_params(doc_params, doc_types, arg_names):
    func_args = []
    args = []
    for name in arg_names:
        func_args.append(Expr(name))
        arg = {'name': name}
        for param in doc_params:
            if name == param.group('name'):
                arg['type'] = param.group('type')
                arg['description'] = param.group('desc')
        for param in doc_types:
            if name == param.group('name'):
                arg['type'] = param.group('type')
        args.append(Input(arg))
    return args, func_args


def call_func_with_builtin_vars(f, *args, **kwargs):
    temp_builtins = {}
    for k, v in builtin_vars.iteritems():
        temp = f.__globals__.get(k)
        if temp is not None:
            temp_builtins[k] = temp
        f.__globals__[k] = v
    try:
        offset = len(stack)
        grab()
        res = f(*args, **kwargs)
        release()
        seq = stack[offset:]
        del stack[offset:]
    finally:
        for k, v in builtin_vars.iteritems():
            temp = temp_builtins.get(k)
            if temp is not None:
                f.__globals__[k] = temp
            else:
                del f.__globals__[k]
    return res, seq


class action(object):
    """
    :type condition: Condition
    :type response: dict
    :type request: Request
    :type thrift_request: ThriftRequest
    """
    condition = None
    response = None
    request = None
    thrift_request = None

    def __init__(self, extends=None):
        self._extends = {'name': extends} if isinstance(extends, basestring) else extends

    def __call__(self, f):
        doc_params = [i for i in param_finder.finditer(f.__doc__)] if f.__doc__ else []
        doc_types = [i for i in type_finder.finditer(f.__doc__)] if f.__doc__ else []
        namespace = None
        if '__doc__' in f.__globals__ and f.__globals__['__doc__']:
            namespace = extract_ns(f.__globals__['__doc__'])
        arg_names = [f.__code__.co_varnames[i] for i in range(f.__code__.co_argcount)]
        args, func_args = augment_params(doc_params, doc_types, arg_names)

        a = ActionDefinition(namespace=namespace)
        a.response = Expr('response')

        temp_action = f.__globals__.get('action')
        f.__globals__['action'] = a
        res, _ = call_func_with_builtin_vars(f, *func_args)
        f.__globals__['action'] = temp_action

        a.input = args
        a.name = f.__name__
        if self._extends:
            a.extends = self._extends
        a.output = res
        a.validate()
        return a


def workflow(description=None, main=None):
    def decorator(f):
        doc_params = [i for i in param_finder.finditer(f.__doc__)] if f.__doc__ else []
        doc_types = [i for i in type_finder.finditer(f.__doc__)] if f.__doc__ else []
        namespace = None
        if '__doc__' in f.__globals__ and f.__globals__['__doc__']:
            namespace = extract_ns(f.__globals__['__doc__'])
        arg_names = [f.__code__.co_varnames[i] for i in range(f.__code__.co_argcount)]
        args, func_args = augment_params(doc_params, doc_types, arg_names)

        scope.namespace = namespace
        res, seq = call_func_with_builtin_vars(f, *func_args)
        scope.namespace = None

        res = res if type(res) is dict else {}
        wf = WorkflowDefinition(namespace=namespace, main=main)
        wf.input = args
        wf.name = f.__name__
        wf.description = description
        wf.output = res
        wf.workflow = seq
        wf.validate()
        return wf

    return decorator


default_vals = dict(response_code=200,
                    execution_id='dfa9f323-fda0-4135-a4c5-dc9c32566d96',
                    response={},
                    body={},
                    workflow_user='robot@fakedomain.com',
                    signaling_msg='not sure what this should be')


def expr():
    def decorator(f):
        arg_names = f.__code__.co_varnames[:f.__code__.co_argcount]
        varargs = [Expr(arg_name) for arg_name in arg_names]
        expression = call_func_with_builtin_vars(f, *varargs, **{})[0]
        serialized_expression = serialize_expression(expression)

        def testable_func(*args, **kwargs):
            test_scope = kwargs.pop('test_scope', False)
            if test_scope is not False:
                scope = {}
                scope.update(default_vals)
                scope.update(test_scope)
                scope.update(kwargs)
                if args:
                    scope.update(dict(zip(arg_names, args)))
                return run_code(serialized_expression, scope)
            else:
                return call_func_with_builtin_vars(f, *args, **kwargs)[0]

        return testable_func

    return decorator
