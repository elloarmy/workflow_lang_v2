import uuid

import json
from schematics.exceptions import ModelValidationError
from schematics.models import Model
from schematics.types import (
    IntType,
    StringType
)
from schematics.types.compound import (
    DictType,
    ListType,
    ModelType
)

from workflow_lang.expressions import Expr
from workflow_lang.types import (
    ExpressionMapType,
    ExpressionType,
    ListExpressionMapType,
    SelfReferentialModelType,
    SimpleModelType
)
from translator.reserved import (
    ACTION,
    DO,
    DYNAMIC_PARALLEL,
    EXTENDS,
    IF,
    ITERATE,
    ITERATE_COLLECTION,
    ITERATE_VARIABLE,
    MAX_RETRIES,
    NAME,
    PARALLEL,
    REQUEST,
    SEQUENTIAL,
    THRIFT_REQUEST
)

"""
stack to keep the different control flow statements. Statements are added when they are instantiated
and popped when they are added to a block (which happens in workflow_lang.decorators)
"""
stack = []
semaphore = 0


def grab():
    global semaphore
    semaphore += 1


def release():
    global semaphore
    semaphore -= 1


class Scope(object):
    __slots__ = 'namespace'


scope = Scope()
scope.namespace = None


class CallExpr(Expr):
    def __init__(self, call, **kwargs):
        id = call.replace('.', '_') + uuid.uuid4().hex
        super(CallExpr, self).__init__(id)
        self.__dict__[id] = Call({'call': call, 'args': kwargs, 'name': id})


class BaseModel(Model):
    class Options:
        serialize_when_none = False  # Should only serialize when None in few situations


class Statement(BaseModel):
    def __init__(self, raw_data=None, deserialize_mapping=None, strict=True):
        super(Statement, self).__init__(raw_data, deserialize_mapping, strict)
        if semaphore > 0:
            stack.append(self)


class Callable(BaseModel):
    def __call__(self, *args, **kwargs):
        inputargs = [i.name for i in self.input]
        try:
            inputargs += [i.name for i in self.extends.input if
                          i.name not in inputargs]
        except AttributeError:
            pass  # This fails if self.extend.input is bad in anyway, which is fine (it could be str
            # or None or without .input)

        if len(kwargs) + len(args) != len(inputargs):
            raise Exception('Bad input for action - wrong amount of arguments')

        if not set(kwargs.keys()).issubset(inputargs):
            raise Exception(
                'Bad input for action - keys should be: {}'.format(', '.join(inputargs)))

        if len(args) > 0:
            inputargs = filter(lambda i: i not in kwargs.keys(), inputargs)
            kwargs.update({k: v for k, v in zip(inputargs, args)})

        try:
            name = self._namespace + '.' if scope.namespace != self._namespace else ''
            name += self.name
        except AttributeError:
            name = self.name

        return CallExpr(name, **kwargs)


class Input(BaseModel):
    name = StringType(required=True)
    description = StringType()
    type = StringType()

    def to_primitive(self, role=None, context=None):
        prim = super(Input, self).to_primitive(role, context)
        if len(prim) == 1:
            return prim['name']

        return prim


class Condition(BaseModel):
    pre = ExpressionType()
    post = ExpressionType()
    post_feedback = ExpressionType()


class Retry(BaseModel):
    wait = IntType()
    max_retries = IntType(serialized_name=MAX_RETRIES)


class IDL(BaseModel):
    file = StringType()
    service = StringType()


class BaseRequest(BaseModel):
    method = StringType()
    body = ListExpressionMapType()
    headers = ListExpressionMapType()
    retry = ModelType(Retry)

    def __init__(self, raw_data=None, deserialize_mapping=None, strict=True):
        super(BaseRequest, self).__init__(raw_data, deserialize_mapping, strict)
        if not self.retry:
            self.retry = Retry()


class Request(BaseRequest):
    url = StringType()
    parameters = ListExpressionMapType()
    returns = ListType(IntType)


class ThriftRequest(BaseRequest):
    idl = ModelType(IDL)
    service = StringType()

    def __init__(self, raw_data=None, deserialize_mapping=None, strict=True):
        super(ThriftRequest, self).__init__(raw_data, deserialize_mapping, strict)
        if not self.idl:
            self.idl = IDL()


class Extension(BaseModel):
    name = StringType(required=True)
    input = ListType(SimpleModelType(Input), default=[])


class ActionDefinition(Callable):
    name = StringType(required=True)
    extends = SelfReferentialModelType(Extension, 'ActionDefinition')
    input = ListType(SimpleModelType(Input), default=[])
    output = ListExpressionMapType(default={})
    condition = ModelType(Condition)
    request = ModelType(Request)
    thrift_request = ModelType(ThriftRequest)

    def __init__(self, namespace=None, raw_data=None, deserialize_mapping=None, strict=True):
        super(ActionDefinition, self).__init__(raw_data, deserialize_mapping, strict)
        if not self.condition:
            self.condition = Condition()
        if not self.request:
            self.request = Request()
        if not self.thrift_request:
            self.thrift_request = ThriftRequest()
        self._namespace = namespace

    def validate(self, partial=False, strict=False):
        super(ActionDefinition, self).validate(partial=partial, strict=strict)
        self.to_primitive()

    def to_primitive(self, role=None, context=None):
        prim = super(ActionDefinition, self).to_primitive(role, context)
        if THRIFT_REQUEST in prim and REQUEST in prim:
            raise ModelValidationError('Cannot have both request and thrift_request in action')
        res = {}
        if EXTENDS in prim:
            res[EXTENDS] = prim.pop(EXTENDS)[NAME]
        if NAME in prim:
            res[NAME] = prim.pop(NAME)
        res[ACTION] = prim

        if context != 'resource':
            return res

        return {'name': self.name,
                'file': self.name + '.json',
                'type': 'lib',
                'language': 'json',
                'workflow_definition': json.dumps(res)}


class Block(Statement):
    statements = ListType(SimpleModelType(Statement), required=True)

    def __init__(self,
                 parallel=False,
                 raw_data=None,
                 deserialize_mapping=None,
                 strict=True):
        super(Block, self).__init__(raw_data, deserialize_mapping, strict)
        self._parallel = parallel

    def to_primitive(self, role=None, context=None):
        prim = super(Block, self).to_primitive(role, context)
        return {PARALLEL if self._parallel else SEQUENTIAL: prim['statements']}


class Signal(BaseModel):
    action = StringType()
    args = ExpressionMapType()


class Phase(BaseModel):
    key = StringType(required=True)
    ctx = ListType(ExpressionType)
    display_data = ExpressionMapType()
    notification = ExpressionMapType()
    signals = DictType(ModelType(Signal), default={})

    def __init__(self,
                 stmt,
                 raw_data=None,
                 deserialize_mapping=None,
                 strict=True):
        super(Phase, self).__init__(raw_data, deserialize_mapping, strict)
        self._stmt = stmt

    def add_signal(self, signal, action):
        if isinstance(action, CallExpr):
            call = stack.pop(-1)
            self.signals[signal] = {ACTION: call.call, 'args': call.args}
            return self
        else:
            raise ValueError('Action must be Call')

    def to_primitive(self, role=None, context=None):
        prim = super(Phase, self).to_primitive(role, context)
        if 'signals' in prim:
            phase = prim.pop('signals')
            phase['id'] = prim
        else:
            phase = {'id': prim}
        return {self._stmt: phase}


class Call(Statement):
    call = StringType(required=True)
    args = ExpressionMapType()
    name = StringType()
    phase = SimpleModelType(Phase)

    def to_primitive(self, role=None, context=None):
        prim = super(Call, self).to_primitive(role, context)
        if 'phase' in prim:
            phase = prim.pop('phase')
            prim.update(phase)
        return {prim.pop('call'): prim}


class Iteration(Statement):
    var = StringType(required=True, serialized_name=ITERATE_VARIABLE)
    list = ExpressionType(required=True, serialized_name=ITERATE_COLLECTION)
    sequential = ListType(SimpleModelType(Statement), required=True, serialize_when_none=True)
    output = ListExpressionMapType()

    def __init__(self, parallel=False, raw_data=None, deserialize_mapping=None, strict=True):
        super(Iteration, self).__init__(raw_data, deserialize_mapping, strict)
        self._parallel = parallel

    def to_primitive(self, role=None, context=None):
        prim = super(Iteration, self).to_primitive(role, context)
        return {DYNAMIC_PARALLEL if self._parallel else ITERATE: prim}


class Conditional(Statement):
    condition = ExpressionType(required=True, serialized_name=IF)
    sequential = ListType(SimpleModelType(Statement), required=True, serialize_when_none=True)

    def to_primitive(self, role=None, context=None):
        prim = super(Conditional, self).to_primitive(role, context)
        return {DO: prim}


class WorkflowDefinition(Callable):
    workflow = ListType(SimpleModelType(Statement), serialize_when_none=True)
    name = StringType(required=True)
    description = StringType()
    output = ListExpressionMapType()
    input = ListType(SimpleModelType(Input), default=[], serialize_when_none=True)

    def __init__(self, namespace=None, main=None, raw_data=None, deserialize_mapping=None,
                 strict=True):
        super(WorkflowDefinition, self).__init__(raw_data, deserialize_mapping, strict)
        self._namespace = namespace
        self._type = 'main' if main else 'lib'

    def to_primitive(self, role=None, context=None):
        res = super(WorkflowDefinition, self).to_primitive(role=role, context=context)
        if context != 'resource':
            return res

        return {'name': self.name,
                'file': self.name + '.json',
                'type': self._type,
                'language': 'json',
                'workflow_definition': json.dumps(res)}

    def with_type(self, workflow_type):
        self._type = workflow_type
        return self


base = ActionDefinition(raw_data={'name': 'base'})
base_thrift = ActionDefinition(raw_data={'name': 'base_thrift'})
