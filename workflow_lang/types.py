from schematics.exceptions import ValidationError
from schematics.types import BaseType
from schematics.types.compound import (
    DictType,
    ModelType
)

from workflow_lang.expressions import (
    Expr,
    is_serialized_expression
)
from workflow_lang.utils import serialize_expression

expr_classes = (Expr, basestring, int, bool, dict, list, tuple, set)


class ExpressionType(BaseType):
    def validate_expression(self, value):
        if not isinstance(value, expr_classes) and value is not None:
            raise ValidationError('Value must be Expression, not {}'.format(value))

    def to_native(self, value, context=None):
        return serialize_expression(value, context == 'pretty')

    def to_primitive(self, value, context=None):
        return serialize_expression(value, context == 'pretty')


class ExpressionMapType(DictType):
    def __init__(self, **kwargs):
        super(ExpressionMapType, self).__init__(ExpressionType, **kwargs)

    def to_native(self, value, safe=False, context=None):
        if isinstance(value, Expr):
            return serialize_expression(value, context == 'pretty')
        return super(ExpressionMapType, self).to_native(value, safe, context)

    def validate_items(self, items):
        if is_serialized_expression(items):
            self.field.validate(items)
        else:
            super(ExpressionMapType, self).validate_items(items)


class ListExpressionMapType(ExpressionMapType):
    def export_loop(self, dict_instance, field_converter,
                    role=None, print_none=False):
        if isinstance(dict_instance, Expr) or is_serialized_expression(dict_instance):
            return field_converter(self.field, dict_instance)

        d = super(ListExpressionMapType, self).export_loop(dict_instance, field_converter, role,
                                                           print_none)
        if d is None:
            return d
        else:
            return [{k: v} for k, v in d.iteritems()]


class SelfReferentialModelType(ModelType):
    def __init__(self, model_class, enclosing_class, **kwargs):
        self.enclosing_class = enclosing_class

        super(SelfReferentialModelType, self).__init__(model_class, **kwargs)

    def to_native(self, value, mapping=None, context=None):
        if type(value).__name__ == self.enclosing_class:
            return value
        return super(SelfReferentialModelType, self).to_native(value, mapping, context)


class SimpleModelType(BaseType):
    def __init__(self, model_class, **kwargs):
        self.model_class = model_class

        validators = kwargs.pop("validators", [])
        self.strict = kwargs.pop("strict", True)

        def validate_model(model_instance):
            model_instance.validate()
            return model_instance

        super(SimpleModelType, self).__init__(validators=[validate_model] + validators, **kwargs)

    def validate_simple_model(self, value):
        if not isinstance(value, self.model_class):
            raise ValidationError(
                'Value must be {classname}, not {value}'.format(classname=self.model_class.__name__,
                                                                value=value))
        value.validate()

    def to_primitive(self, value, context=None):
        return value.to_primitive(context=context)
