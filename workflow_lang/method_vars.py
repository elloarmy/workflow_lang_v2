from workflow_lang.expressions import (
    GlobalsExpr,
    Expr
)

"""Builtin vars injected into workflows, so expressions are captured instead of executed"""

builtin_vars = dict(response_code=Expr('response_code'),
                    response=Expr('response'),
                    execution_id=Expr('execution_id'),
                    body=Expr('body'),
                    uuid4=Expr('uuid4'),
                    str=GlobalsExpr('str'),
                    any=GlobalsExpr('any'),
                    all=GlobalsExpr('all'),
                    set=GlobalsExpr('set'),
                    list=GlobalsExpr('list'),
                    len=GlobalsExpr('len'),
                    dict=GlobalsExpr('dict'),
                    workflow_user=Expr('workflow_user'),
                    signaling_msg=Expr('signaling_msg'),
                    globals=GlobalsExpr('globals'),
                    int=GlobalsExpr('int'),
                    range=GlobalsExpr('range'),
                    xrange=GlobalsExpr('xrange'),
                    utcfromtimestamp=Expr('utcfromtimestamp'),
                    utcnow=Expr('utcnow'),
                    timedelta=Expr('timedelta'),
                    urlencode=Expr('urlencode'))
