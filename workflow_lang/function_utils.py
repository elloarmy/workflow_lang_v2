import uuid

from workflow_lang.expressions import (
    ListExpr,
    OperatorExpr,
    StringExpr,
    Expr
)
from workflow_lang.models import Iteration


def GenericMap(workflow, list, parallel=False):
    iter = Iteration(parallel)
    iter.list = list
    iter.sequential = workflow.workflow
    iter.var = workflow.input[0].name if len(workflow.input) > 0 else 's' + uuid.uuid4().hex
    iter.output = workflow.output
    iter.validate()
    return {k: Expr(k) for k in iter.output.keys()}


def MapAsync(workflow, list):
    return GenericMap(workflow, list, True)


def Map(workflow, list):
    return GenericMap(workflow, list, False)


def Format(string, *args, **kwargs):
    return StringExpr(string).format(*args, **kwargs)


def Join(string, *args, **kwargs):
    return StringExpr(string).join(*args, **kwargs)


def List(expr, list, cond=lambda *_: True):
    """
    :param expr:
    :type expr: function
    :param list:
    :param cond:
    :type cond: function
    :return:
    """
    vars = expr.__code__.co_varnames
    return ListExpr(expr, (vars, lambda: list), cond=cond)


def And(left, right):
    return OperatorExpr(left, right, 'and')


def Or(left, right):
    return OperatorExpr(left, right, 'or')


def In(element, list):
    return OperatorExpr(element, list, 'in')


def Is(left, right):
    return OperatorExpr(left, right, 'is')
