import re

import json
import yaml

from translator.reserved import (
    ACTION,
    CONDITION,
    DESCRIPTION,
    DO,
    DYNAMIC_PARALLEL,
    EXTENDS,
    IF,
    INPUT,
    ITERATE,
    ITERATE_COLLECTION,
    ITERATE_VARIABLE,
    NAME,
    OUTPUT,
    PARALLEL,
    PHASE_HEADER,
    PHASE_KEY,
    REQUEST,
    SEQUENTIAL,
    THRIFT_REQUEST,
    WORKFLOW
)
from translator.expression_parser import (
    dupe_names,
    handle_possible_expression
)
from translator.templates import (
    action_template,
    block_template,
    call_template,
    conditional_template,
    doc_templ,
    imports_template,
    iterate_template,
    reStructuredText,
    workflow_template
)


def remove_hyphens_and_camelcase(name):
    return re.sub('(-[a-z])', lambda x: x.group(0)[1].upper(), name)


def possible_dict(m, indent=0):
    if isinstance(m, list):
        def extract_key_value(x):
            (k, v), = x.items()
            return '\'{}\''.format(k) + ': ' + handle_possible_expression(v)

        return '{{{}}}'.format((',\n' + " " * indent).join(map(extract_key_value, m)))
    return m


def format_request_parameters(k, v):
    if k in ['url', 'method']:
        return "'{}'".format(v)
    elif k == 'body':
        return possible_dict(v, 27)
    elif k == 'parameters':
        return possible_dict(v, 34)
    elif k == 'headers':
        return possible_dict(v, 31)
    else:
        return v


def format_action_body(a):
    prefix = '    {}.'.format(ACTION)
    statements = []

    # Handle request attributes
    request = a.get(REQUEST) or a.get(THRIFT_REQUEST, {})
    templ = prefix + REQUEST + '.{k} = {v}'
    statements += [templ.format(k=k, v=format_request_parameters(k, v)) for k, v in
                   request.iteritems()]

    # Handle conditions
    condition = a.get(CONDITION, {})
    templ = prefix + CONDITION + '.{k} = {v}'
    statements += [templ.format(k=k, v=handle_possible_expression(v)) for k, v in
                   condition.iteritems()]

    # Handle output
    output = a.get(OUTPUT, None)
    if output:
        statements.append('    return {}'.format(possible_dict(output, indent=12)))

    return '\n'.join(statements) if statements else '    pass'


def format_input(input):
    """
    Collect details on input and format it as reStructuredText (docstring)
    """
    input_names = []
    input_metadata = []
    for i in input:
        if isinstance(i, dict):
            input_name = i.pop('name')
            input_names.append(input_name)
            if i:
                t = ' ' + i['type'] if 'type' in i else ''
                desc = ' ' + i['description'] if 'description' in i else ''
                input_metadata.append(reStructuredText(type=t, name=input_name, desc=desc))
        else:
            input_names.append(i)
    return input_names, input_metadata


def make_action(d, name):
    a = d[ACTION]

    input_names, input_metadata = format_input(a.get(INPUT, []))

    stmts = format_action_body(a)

    text = action_template(thrift='thrift_' if THRIFT_REQUEST in a else '',
                           extends=remove_hyphens_and_camelcase(d.get(EXTENDS, '')),
                           name=remove_hyphens_and_camelcase(name),
                           stmts=stmts,
                           input=', '.join(input_names),
                           docs=doc_templ(''.join(input_metadata)) if input_metadata else '')
    return text


dupe_assignments = []


def make_workflow(d, name, main):
    input_names, inputmetadata = format_input(d.get(INPUT, []))

    params = []

    description = d.get(DESCRIPTION, None)
    if description is not None:
        params.append(DESCRIPTION + '="{}"'.format(description))
    if main:
        params.append('main=True')

    seq = visit_sequence(d[WORKFLOW], 1)

    output = d.get(OUTPUT, None)
    if output:
        seq.append('    return {}'.format(possible_dict(output, indent=12)))

    seq = "\n".join(seq) if seq else '    pass'

    text = workflow_template(name=remove_hyphens_and_camelcase(name),
                             input=', '.join(input_names),
                             params=', '.join(params),
                             seq=seq,
                             docs=doc_templ(''.join(inputmetadata)) if inputmetadata else '')

    return text


def visit_cond(d, indentation):
    seq = visit_sequence(d[SEQUENTIAL], indentation + 1)

    text = conditional_template(cond=handle_possible_expression(d[IF]),
                                ind=' ' * indentation * 4,
                                seq="\n".join(seq))
    return text


def visit_loop(d, indentation, parallel):
    seq = visit_sequence(d[SEQUENTIAL], indentation + 1)
    output = d.get(OUTPUT, [])
    tmpl = '{k} = LoopAsync.outputs({v})'
    returns = [(indentation + 1) * 4 * ' ' + tmpl.format(k=k, v=handle_possible_expression(v)) for
               o in output for k, v in o.items()]
    text = iterate_template(list=handle_possible_expression(d[ITERATE_COLLECTION]),
                            parallel='LoopAsync' if parallel else 'Loop',
                            var=d[ITERATE_VARIABLE],
                            ind=' ' * indentation * 4,
                            seq="\n".join(seq),
                            returns='\n'.join(returns))

    return text


def visit_block(d, indentation, parallel):
    seq = visit_sequence(d, indentation + 1)

    text = block_template(parallel='Parallel' if parallel else 'Sequential',
                          ind=' ' * indentation * 4,
                          seq="\n".join(seq))
    return text


def handle_id_dict(d):
    return ', '.join(["'{k}': {v}".format(k=k, v=handle_possible_expression(v)) for k, v
                      in d.iteritems()])


def handle_phase_args(args):
    res = ["key='{}'".format(args[PHASE_KEY])]
    if 'context' in args:
        res.append('context={}'.format(handle_possible_expression(args['context'])))
    if 'display-data' in args:
        res.append('display_data={{{}}}'.format(handle_id_dict(args['display-data'])))
    if 'notification' in args:
        res.append('notification={{{}}}'.format(handle_id_dict(args['notification'])))
    return ', '.join(res)


def visit_phase(elem, indentation):
    prefix = '\n' + ' ' * indentation * 4
    res = []
    for p, conf in elem.iteritems():
        combined_args = handle_phase_args(conf.pop(PHASE_HEADER))
        signal_templ = ".add_signal('{s}', {a})"
        signals = ''.join([signal_templ.format(s=k, a=visit_call({v.pop(ACTION): v}, 0)) for k, v
                           in conf.iteritems()])

        res.append(prefix + p.replace('-', '_') + '({})'.format(combined_args) + signals)
    return ''.join(res)


def visit_call(elem, indentation):
    (name, conf), = elem.items()
    if not conf:
        conf = {}
    name = remove_hyphens_and_camelcase(name)
    namespace = name.split('.')[0] if '.' in name else -1
    if NAME in conf:
        ret_name = conf.pop(NAME).replace('-', '_')

        # make sure we assign with same id if variable name has already been assigned to
        if ret_name in dupe_assignments:
            delim = ' <<= '
        else:
            dupe_assignments.append(ret_name)
            delim = ' = '

        # change variable name and all occurences of variable name if import or function is same
        if ret_name == namespace:
            ret_name += '_'
            dupe_names.append(namespace)
        elif ret_name == name:
            ret_name += '_'
            dupe_names.append(name)
        ret_name += delim
    else:
        ret_name = ''

    if 'args' in conf:
        argsep = ',\n' + ' ' * (indentation * 4 + len(ret_name) + len(name) + 1)
        args = argsep.join(map(lambda (k, v): k + '=' + handle_possible_expression(v),
                               conf.pop('args').iteritems()))
    else:
        args = ''

    phase = visit_phase(conf, indentation)
    if phase and name == 'stdlib.noop':
        return phase
    text = call_template(ind=' ' * indentation * 4,
                         name=name,
                         return_name=ret_name,
                         args=args,
                         phase=phase)
    return text


visitors = {DO: visit_cond,
            ITERATE: lambda e, i: visit_loop(e, i, False),
            DYNAMIC_PARALLEL: lambda e, i: visit_loop(e, i, True),
            SEQUENTIAL: lambda e, i: visit_block(e, i, False),
            PARALLEL: lambda e, i: visit_block(e, i, True)}


def visit_sequence(seq, indentation):
    if not seq:
        return []
    res_seq = []
    for elem in seq:
        [(key, conf)] = elem.items()
        res_seq.append(visitors.get(key, lambda _, i: visit_call(elem, i))(conf, indentation))
    return res_seq


def translate_yaml(filepath, name, main):
    del dupe_names[:]
    del dupe_assignments[:]
    d = yaml.loads(open(filepath, 'r').read())
    if ACTION in d:
        return make_action(d, name)
    elif WORKFLOW in d:
        return make_workflow(d, name, main)


def iterate_through_directory(path):
    print """

    ### {}

    """.format(path)

    with open(path + 'manifest.json') as f:
        manifest = json.load(f)

    imports = ["\nimport udeploy_workflows_ported." + i['bundle'] + ' as ' + i['bundle']
               for i in manifest.pop('imports', [])]

    resources = manifest.pop('resources')

    manifest['maintainers'] = [i.encode('utf-8') for i in manifest['maintainers']]

    info = [k + ': ' + str(v) for k, v in manifest.iteritems()]

    with open(path + '__init__.py', 'w') as f:
        f.write(imports_template(imports=''.join(imports + ['']),
                                 info='\n'.join(info)))
        for file in resources:
            print file['name']
            text = translate_yaml(path + file['file'], file['name'], file['type'] == 'main')
            # handle string.format
            text = re.sub(r'(u?(?:\'[^\']*\'|"[^"]*")).format\(', r'Format(\g<1>, ', text)
            # handle string.join
            text = re.sub(r'(u?(?:\'[^\']*\'|"[^"]*")).join\(', r'Join(\g<1>, ', text)
            # handle string %
            text = re.sub(r'(u?(?:\'[^\']*\'|"[^"]*")) %', r'StringVar(\g<1>) %', text)
            f.write(text)
