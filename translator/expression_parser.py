import parser
from symbol import (
    and_expr,
    and_test,
    arglist,
    argument,
    arith_expr,
    atom,
    comp_for,
    comp_op,
    comparison,
    dictorsetmaker,
    exprlist,
    factor,
    list_for,
    list_if,
    listmaker,
    not_test,
    or_test,
    sliceop,
    subscript,
    sym_name,
    term,
    test,
    testlist_comp,
    trailer
)
from token import (
    ISTERMINAL,
    tok_name
)


dupe_names = []

func = 341
sym_name[341] = 'func'


def process_list(t):
    if type(t) is not list:
        return t
    if len(t) == 2 and type(t[1]) is list:
        if ISTERMINAL(t[1][0]):
            return t
        else:
            return process_list(t[1])
    elif len(t) > 2:
        if t[0] == 317:
            if 36 not in zip(*t[1:])[0]:
                return [func] + [process_list(e) for e in t[1:]]
        return [process_list(e) for e in t]
    return t


# Used for debugging
def replace_codes(t):
    if type(t) is int:
        if t > 255:
            return sym_name[t]
        else:
            return tok_name[t]
    elif type(t) is not list:
        return t
    for i in range(len(t)):
        t[i] = replace_codes(t[i])
    return t


# Used for debugging
def reconstruct_code(t):
    if type(t) not in (tuple, list):
        if t in ['for', 'in', 'is', '==', 'and', '&', '|']:
            t = ' ' + t + ' '
        elif t == ',':
            t += ' '
        return t

    return ''.join([reconstruct_code(elem) for elem in t[1:]])


def visit_expr_var(var):
    if len(var) == 4 and var[2][0] == listmaker and var[2][-1][0] == list_for:
        return visit_expr(var[2])
    elif len(var) == 4 and var[2][0] == dictorsetmaker and var[2][-1][0] == comp_for:
        return visit_expr(var[2])
    elif len(var) == 3 and var[2][0] == comp_for:
        var[2][0] = list_for
        return visit_expr_loop(var)
    return ''.join(map(lambda x: visit_expr(x), var[1:]))


def visit_expr_boolean(boolean):
    return 'BooleanVar(' + visit_expr(boolean[3]) + ', ' + visit_expr(
        boolean[1]) + ', ' + visit_expr(boolean[5]) + ')'


def visit_expr_comparison(comparison):
    left = visit_expr(comparison[1])
    operator = visit_expr(comparison[2])
    right = comparison[3:]
    right = visit_expr(right[0] if len(right) == 1 else [comparison[0]] + right)

    if operator in ('and', 'or', 'in', 'is', 'not in', 'is not'):
        return 'OperatorVar(' + left + ', ' + right + ', "' + operator + '")'

    return left + ' ' + operator + ' ' + right


def visit_expr_loop(loop):
    if loop[-1][0] != list_for:
        return ''.join(map(lambda x: visit_expr(x), loop[1:]))
    expr = visit_expr(loop[1])
    vars = []
    lists = []
    while loop[-1][0] == list_for:
        loop = loop[-1]
        vars.append(visit_expr(loop[2]))
        lists.append('lambda ' + ', '.join(vars[:-1]) + ': ' + visit_expr(loop[4]))

    cond = ''
    if len(loop) > 0 and loop[-1][0] == list_if:
        cond = ', cond=lambda ' + ', '.join(vars) + ': ' + visit_expr(loop[-1][2])
    return 'ListVar(lambda ' + ', '.join(vars) + ': ' + expr + ', ' + ','.join(
        map(lambda l: '(' + ', '.join(l) + ')',
            zip(map(lambda var: "['" + var.replace(",", "', '") + "']", vars), lists))) + cond + ')'


def visit_expr_dict(dict):
    if dict[-1][0] != comp_for:
        return ''.join(map(lambda x: visit_expr(x), dict[1:]))
    k = visit_expr(dict[1])
    v = visit_expr(dict[3])
    vars = []
    lists = []
    while dict[-1][0] == comp_for:
        dict = dict[-1]
        vars.append(visit_expr(dict[2]))
        lists.append('lambda ' + ', '.join(vars[:-1]) + ': ' + visit_expr(dict[4]))
    strvars = map(lambda var: "['" + var.replace(",", "', '") + "']", vars)

    return 'DictVar(lambda ' + ', '.join(vars) + ': ' + k + ', ' + 'lambda ' + ', '.join(
        vars) + ': ' + v + ', ' + ','.join(map(lambda l: '(' + ', '.join(l) + ')',
                                               zip(strvars, lists))) + ')'


def visit_expr(expr):
    t = expr[0]
    if ISTERMINAL(t):
        t = expr[1]
        if t == 'response':
            t = 'action.' + t
        elif t in dupe_names:
            t += '_'
        return t
    elif t == comp_op:
        return ' '.join([visit_expr(e) for e in expr[1:]])
    elif t == listmaker:
        return visit_expr_loop(expr)
    elif t in [func, atom, trailer, arglist, exprlist, argument, testlist_comp, subscript, sliceop,
               factor]:
        return visit_expr_var(expr)
    elif t in [comparison, or_test, and_test, and_expr, term, arith_expr]:
        return visit_expr_comparison(expr)
    elif t == not_test:
        return 'Not(' + visit_expr(expr[2]) + ')'
    elif t == test:
        return visit_expr_boolean(expr)
    elif t == dictorsetmaker:
        return visit_expr_dict(expr)

    raise ValueError('symname %s is missing' % (sym_name[t]))


def handle_possible_expression(expr):
    if type(expr) is not str:
        return str(expr)
    try:
        d = process_list(parser.expr(expr).tolist())
        return visit_expr(d[1]).replace('**', 'unpack_dict=').replace('*', 'unpack_list=')
    except (ValueError, SyntaxError) as e:
        return expr + '  # TODO check what failed with this expression'
