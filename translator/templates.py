def action_template(thrift, extends, name, input, stmts, docs):
    return """

@{thrift}action({extends})
def {name}({input}):{docs}
{stmts}
""".format(thrift=thrift,
           extends=extends,
           name=name,
           stmts=stmts,
           input=input,
           docs=docs)


def workflow_template(params, name, input, seq, docs):
    return """

@workflow({params})
def {name}({input}):{docs}
{seq}
""".format(name=name,
           input=input,
           params=params,
           seq=seq,
           docs=docs)


def conditional_template(ind, cond, seq):
    return """{ind}with If({cond}):
{seq}
""".format(cond=cond,
           ind=ind,
           seq=seq)


def iterate_template(ind, parallel, list, var, seq, returns):
    return """{ind}with {parallel}({list}) as {var}:
{seq}
{returns}
""".format(list=list,
           parallel=parallel,
           var=var,
           ind=ind,
           seq=seq,
           returns=returns)


def block_template(ind, parallel, seq):
    return """{ind}with {parallel}():
{seq}
""".format(parallel=parallel,
           ind=ind,
           seq=seq)


def call_template(ind, return_name, name, args, phase):
    return "{ind}{return_name}{name}({args}){phase}".format(ind=ind,
                                                            name=name,
                                                            return_name=return_name,
                                                            args=args,
                                                            phase=phase)


def reStructuredText(type, name, desc):
    """
    Template for parameter annotations in docstring
    """
    return '\n    :param{type} {name}:{desc}'.format(type=type, name=name, desc=desc)


def doc_templ(docs):
    return '\n    """{}\n    """'.format(docs)


def imports_template(info, imports):
    return '''"""
{info}
"""
from workflow_lang.frontend import (
    Else,
    Format,
    If,
    Join,
    LegacyCall,
    List,
    Loop,
    LoopAsync,
    Map,
    MapAsync,
    Not,
    Parallel,
    Sequential,
    action,
    base,
    base_thrift,
    expr,
    phase_cancel,
    phase_complete,
    phase_fail,
    phase_pause,
    phase_rollback,
    phase_start,
    phase_update,
    workflow
)
from workflow_lang.globals import *
from workflow_lang.expressions import (
    BooleanExpr,
    SimpleDictExpr,
    StringExpr
){imports}
'''.format(imports=imports,
           info=info)
