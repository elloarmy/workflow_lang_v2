DESCRIPTION = 'description'
NAME = 'name'

INPUT = 'input'
OUTPUT = 'output'

WORKFLOW = 'workflow'
EXTENDS = 'extends'
SEQUENTIAL = 'sequential'
PARALLEL = 'parallel'
DO = 'do'
IF = 'if'

DYNAMIC_PARALLEL = 'run_concurrently'
ITERATE = 'iterate'
ITERATE_VARIABLE = 'for'
ITERATE_COLLECTION = 'in'

ACTION = 'action'

CONDITION = 'condition'
PRE = 'pre'
POST = 'post'

REQUEST = 'request'
THRIFT_REQUEST = 'thrift_request'

MAX_RETRIES = 'max-retries'
WAIT = 'wait'

PARAMETERS = 'parameters'

PHASE_HEADER = 'id'
PHASE_KEY = 'key'
PHASE_START = 'phase-start'
PHASE_COMPLETE = 'phase-complete'
PHASE_ROLLBACK = 'phase-rollback'
PHASE_PAUSE = 'phase-pause'
PHASE_CANCEL = 'phase-cancel'
PHASE_FAIL = 'phase-fail'

PHASE_UPDATE = 'phase-update'
